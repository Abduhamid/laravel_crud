<?php
Breadcrumbs::register('home', function($breadcrumbs)
{
    $breadcrumbs->push('Dashboard', route('home'));
});

require(__DIR__.'\Breadcrumbs\Card.php');
require(__DIR__.'\Breadcrumbs\Category.php');