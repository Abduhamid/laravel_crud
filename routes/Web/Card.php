<?
Route::group(['namespace' => 'Card'], function (){
    Route::resource('card', 'CardController', ['except' => ['destroy']]);
    Route::get('card/{id}/delete', ['as' => 'card.delete', 'uses' => 'CardController@destroy']);
});

