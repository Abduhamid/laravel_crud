<?php

Breadcrumbs::register('card.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Card', route('card.index'));
});
Breadcrumbs::register('card.create', function ($breadcrumbs) {
    $breadcrumbs->parent('card.index');
    $breadcrumbs->push('Create', route('card.create'));
});
Breadcrumbs::register('card.edit', function($breadcrumbs, $card) {
    $breadcrumbs->parent('card.index');
    $breadcrumbs->push($card??'', route('card.edit', $card));
    $breadcrumbs->push('Edit', '');
});
Breadcrumbs::register('card.show', function($breadcrumbs, $card) {
    $breadcrumbs->parent('card.index');
    $breadcrumbs->push($card??'', route('card.show', $card));
    $breadcrumbs->push('Show', '');
});