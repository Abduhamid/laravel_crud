<?php
Breadcrumbs::register('category.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Category', route('category.index'));
});
Breadcrumbs::register('category.create', function ($breadcrumbs) {
    $breadcrumbs->parent('category.index');
    $breadcrumbs->push('Create', route('category.create'));
});
Breadcrumbs::register('category.edit', function($breadcrumbs, $category) {
    $breadcrumbs->parent('category.index');
    $breadcrumbs->push($category??'',  route('category.edit', $category));
    $breadcrumbs->push('Edit', '');
});
Breadcrumbs::register('category.show', function($breadcrumbs, $category) {
    $breadcrumbs->parent('category.index');
    $breadcrumbs->push($category??'', route('category.show', $category));
    $breadcrumbs->push('Show', '');
});