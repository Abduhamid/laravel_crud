<div class="form-group required">
    {!! Form::label('name', 'Name:', ['class' => 'control-label col-sm-2']) !!}
    <div class='col-sm-9'>
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="form-group required">
    {!! Form::label('year', 'Year:', ['class' => 'control-label col-sm-2']) !!}
    <div class='col-sm-9'>
        {!! Form::date('year', \Carbon\Carbon::now(), ['class' => 'form-control']) !!}
    </div>
</div>
<div class="form-group required">
    {!! Form::label('pan', 'Pan:', ['class' => 'control-label col-sm-2']) !!}
    <div class='col-sm-9'>
        {!! Form::text('pan', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="form-group required">
    {!! Form::label('price', 'Price:', ['class' => 'control-label col-sm-2']) !!}
    <div class='col-sm-9'>
        {!! Form::number('price', null, ['class' => 'form-control', 'step'=>'any']) !!}
    </div>
</div>
<div class="form-group required">
    {!! Form::label('category', 'Category:', ['class' => 'control-label col-sm-2']) !!}
    <div class='col-sm-9'>
        {!! Form::select    ('category_id', \App\Models\Category\Category::pluck('name', 'id'), $card->category_id, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="form-group required">
    {!! Form::label('is_active', 'Is_Active:', ['class' => 'control-label col-sm-2']) !!}
    <div class='col-sm-9'>
        {!! Form::select('is_active', trans('InterfaceTranses.enabled'), $card->is_active??null, ['class' => 'form-control']) !!}
    </div>
</div>