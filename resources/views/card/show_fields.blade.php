<div class="form-group">
    {!! Form::label('name', 'Name:', ['class' => 'control-label col-sm-2']) !!}
    <div class='col-sm-9'>
        <p class="form-control-static">{{$card->name}}</p>
    </div>
</div>
<div class="form-group">
    {!! Form::label('pan', 'Pan:', ['class' => 'control-label col-sm-2']) !!}
    <div class='col-sm-9'>
        <p class="form-control-static">{{ $card->pan }}</p>
    </div>
</div>
<div class="form-group">
    {!! Form::label('price', 'Price:', ['class' => 'control-label col-sm-2']) !!}
    <div class='col-sm-9'>
        <p class="form-control-static">{{ $card->price }}</p>
    </div>
</div>
<div class="form-group">
    {!! Form::label('category', 'Category:', ['class' => 'control-label col-sm-2']) !!}
    <div class='col-sm-9'>
        <p class="form-control-static">{{ $card->category->name }}</p>
    </div>
</div>
<div class="form-group">
    {!! Form::label('year', 'Date:', ['class' => 'control-label col-sm-2']) !!}
    <div class='col-sm-9'>
        <p class="form-control-static">{{ $card->year }}</p>
    </div>
</div>
<div class="form-group">
    {!! Form::label('is_active', 'is_active:', ['class' => 'control-label col-sm-2']) !!}
    <div class='col-sm-9'>
        <p class="form-control-static">{{ trans('InterfaceTranses.enabled.'.$card->is_active) }}</p>
    </div>
</div>


<div class="form-group">
    {!! Form::label('created_at', 'Created_at:', ['class' => 'control-label col-sm-2']) !!}
    <div class='col-sm-9'>
        <p class="form-control-static">{{ $card->created_at }}</p>
    </div>
</div>
<div class="form-group">
    {!! Form::label('updated_at', 'Updated_at:', ['class' => 'control-label col-sm-2']) !!}
    <div class='col-sm-9'>
        <p class="form-control-static">{{ $card->updated_at }}</p>
    </div>
</div>