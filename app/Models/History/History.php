<?php

namespace App\Models\History;


use App\Models\BaseModel;

class History extends BaseModel
{
    protected $table='history';
    protected $primaryKey='id';
}
