<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Parent_;

class BaseModel extends Model
{

    private $old_attributes = null;
    private $new_attributes=null;

    public function getOldAttributes()
    {
        return $this->old_attributes;
    }

    public function setOldAttributes($old_attributes)
    {
        $this->old_attributes=$old_attributes;
    }

    public function getChanges()
    {
        return $this->new_attributes??parent::getChanges();
    }

    public function setChanges($new_attributes)
    {
        $this->new_attributes=$new_attributes;
    }

}