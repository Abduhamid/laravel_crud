<?php

namespace App\Models\Card;

use App\Models\BaseModel;
use App\Models\Category\Category;

class Card extends BaseModel
{
    protected $table='card';
    protected $primaryKey='id';
    protected $fillable=[
        'name',
        'category_id',
        'pan',
        'year',
        'price',
        'is_active',
    ];


    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }
}
