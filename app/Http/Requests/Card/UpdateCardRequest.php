<?php

namespace App\Http\Requests\Card;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCardRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|max:255',
            'pan'=>'required|max:255',
            'price'=>'required|numeric',
            'year'=>'required|date',
            'category_id'=>'required|integer|exists:category,id',
            'is_active'=>'required|boolean',
        ];
    }
}
