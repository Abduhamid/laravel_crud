<?php

namespace App\Http\Controllers\Card;

use App\Events\History\HistoryEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\Card\StoreCardRequest;
use App\Http\Requests\Card\UpdateCardRequest;
use App\Repositories\Card\CardRepositoryContract;
use Illuminate\Http\Request;

class CardController extends Controller
{
    private $cardRepositoryContract;

    /**
     * CardController constructor.
     * @param CardRepositoryContract $cardRepositoryContract
    auth    */
    public function __construct(CardRepositoryContract $cardRepositoryContract)
    {
        $this->cardRepositoryContract=$cardRepositoryContract;
        $this->middleware('card.can-list', ['only'=>['index']]);
        $this->middleware('card.can-show', ['only'=>['show']]);
        $this->middleware('card.can-create', ['only'=>['create', 'store']]);
        $this->middleware('card.can-edit', ['only'=>['update', 'edit']]);
        $this->middleware('card.can-delete', ['only'=>['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cards=$this->cardRepositoryContract->all('');
        return view('card.index', compact('cards'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('card.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCardRequest $request)
    {
        $card=$this->cardRepositoryContract->create($request->validated());
        event(new HistoryEvent($card));
        session()->flash('flash_message', trans('alerts.general.success_add'));
        return redirect()->route('card.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $card=$this->cardRepositoryContract->findById($id);
        $card->load('category');
        return view('card.show', compact('card'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $card=$this->cardRepositoryContract->findById($id);
        return view('card.edit', compact('card'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCardRequest $request, $id)
    {
        $card=$this->cardRepositoryContract->update($request->validated(), $id);
        event(new HistoryEvent($card));
        session()->flash('flash_message', trans('alerts.general.success_edit'));
        return redirect()->route('card.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $card=$this->cardRepositoryContract->destroy($id);
            event(new HistoryEvent($card));
            session()->flash('flash_message',trans('alerts.general.success_delete'));
            return redirect()->route('card.index');
        }catch (\Exception $ex){
            session()->flash('flash_message_error', trans('errorMessages.errors.try_errors.23000'));
            return redirect()->route('card.index');
        }
    }
}
