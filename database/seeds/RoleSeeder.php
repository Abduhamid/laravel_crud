<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('role_user')->insert([
            [
                'role_id' => 1,
                'user_id' => 1,
                'user_type' => 'App\Models\User\User',
            ],
            [
                'role_id' => 1,
                'user_id' => 2,
                'user_type' => 'App\Models\User\User',
            ],
        ]);
    }
}
